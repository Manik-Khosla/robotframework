*** Settings ***
Library           RequestsLibrary
Library           Collections
Library           DateTime

*** Variables ***
${base_url}           https://newsapi.org/v2
${api_key}            f47dd06e46704e83abb04686c21b2db6
${search_keyword}     COVID
${domain}             bbc.co.uk
${page_size}          ${10}
${country}            us

*** Test Cases ***
Get_Covid_News_Info
   [Documentation]        Get all news related to Covid
       create session     mysession   ${base_url}
       ${response}=    GET On Session    mysession   /everything?  params=q=${search_keyword}&domains=${domain}&apiKey=${api_key}&pageSize=${page_size} 
        log to console     ${response.content}  
       
       # Validations
       ${response_status_code}=     convert to string   ${response.status_code}    
       should be equal   ${response_status_code}       200 
       #  Get All articles     
       @{articles}=    Collections.Get From Dictionary    ${response.json()}    articles
       
       FOR  ${article}   IN  @{articles} 
       ${source_name}=       Collections.Get From Dictionary   ${article}[source]      name
       ${title}=     Collections.Get From Dictionary    ${article}    title
       ${description}=     Collections.Get From Dictionary    ${article}      description
       ${url}=     Collections.Get From Dictionary    ${article}      url
       log to console  ---------------------
       log to console   ${source_name}
       log to console   ${title}
       log to console   ${description}
       log to console   ${url}
       # Fields should not be empty 
       should not be empty  ${source_name}
       should not be empty  ${title}
       should not be empty  ${description}
       should not be empty  ${url}
       # Title should contain keyword 
       # should contain   ${title}    ${search_keyword}
       # URL match domain 
       should contain   ${url}    ${domain}   
       END
       # Validate Number of articles returned match pageSize    
       ${article_count} =	Get Length	${articles}
       Should Be Equal As Integers    ${article_count}  ${page_size}  
       log to console  -----------------------


Get_Top_Headlines
    [Documentation]            Get top headlines
     create session     mysession   ${base_url}
     ${response}=       GET On Session    mysession      /top-headlines  params=country=${country}&apiKey=${api_key}
       log to console     ${response.content}  
       
       # Validations
       ${response_status_code}=     convert to string   ${response.status_code}
       should be equal   ${response_status_code}       200      
       # Get All articles
        @{articles}=    Collections.Get From Dictionary    ${response.json()}    articles

        FOR  ${article}  IN    @{articles}
       ${source_name}=       Collections.Get From Dictionary   ${article}[source]      name
       ${title}=     Collections.Get From Dictionary    ${article}    title
       ${description}=     Collections.Get From Dictionary    ${article}      description
       ${url}=     Collections.Get From Dictionary    ${article}      url
       ${publish_date}=     Collections.Get From Dictionary    ${article}      publishedAt
       log to console  ---------------------
       log to console   ${source_name}
       log to console   ${title}
       log to console   ${description}
       log to console   ${url}
       log to console   ${publish_date}
       # Fields should not be empty 
       should not be empty  ${source_name}
       should not be empty  ${title}
       should not be empty  ${description}
       should not be empty  ${url}
       #  Validate Published date
       ${current_date}=   Get Current Date     UTC   exclude_millis=yes   result_format=%Y-%m-%d
       ${publish_date} =	Convert Date	 ${publish_date}   exclude_millis=yes  result_format=%Y-%m-%d
       Should Be Equal	 ${current_date}   ${publish_date}
       END

